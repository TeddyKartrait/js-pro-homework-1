document.addEventListener('DOMContentLoaded', () => {
    // Task 1

    const $dropdownItem = document.querySelectorAll('.dropdown-item-wrapper'),
    $dropdownBtn = document.querySelector('.dropdown-toggle'),
    $dropdownToggle = document.querySelector($dropdownBtn.dataset.target);

    $dropdownBtn.addEventListener('click', event => {
        event._isClickWithinDropdown = true;
        $dropdownToggle.classList.toggle('open')

        removeEvent = event => {
            if (event._isClickWithinDropdown) return;
            $dropdownToggle.classList.remove('open')
            document.body.removeEventListener('click', removeEvent)
        }

        document.body.addEventListener('click', removeEvent)
    })

    $dropdownItem.forEach(el => el.addEventListener('click', event => {
        event._isClickWithinDropdown = true;
        $dropdownToggle.classList.remove('open')
    }))

    if ($dropdownToggle.classList.contains('open')) {
        
    }

    // Task 2

    const $inpLastName = document.querySelector('.js-inp-lastname'),
        $inpName = document.querySelector('.js-inp-name'),
        $inpSurName = document.querySelector('.js-inp-surname'),
        $submitBtn = document.querySelector('.submit-btn');

    $inpLastName.addEventListener('keypress', event => {
        const newValue = event.target.value + event.key,
            valueTreat = newValue.split('')

        valueTreat.forEach(el => {
            if (!el.match(/^[а-яА-Я- ]+$/)) {
                event.preventDefault()
            }
        })
    })

    $inpLastName.addEventListener('blur', () => {
        let validValue = $inpLastName.value,
            mass = [];

        validValue = validValue.split('')

        validValue.forEach(el => {
            if (!el.match(/^[а-яА-Я]+$/)) {
                el = ''
                mass.push(el)
            } else mass.push(el)
        })

        validValue = mass.toString()
        validValue = validValue.replace(/,+/g, '')
            validValue = validValue.replace(/\s+/g, ' ');
            validValue = validValue.replace(/-+/g, '-');
            
        
            if (validValue.at(0) == '-') validValue = validValue.slice(1)
            if (validValue.at(-1) == '-') validValue = validValue.slice(0, validValue.length - 1)
            validValue = validValue.trim()
            validValue = (validValue.substring(0, 1).toUpperCase() + (validValue.substring(1)).toLowerCase());
        $inpLastName.value = validValue
    })

    $inpName.addEventListener('keypress', event => {
        const newValue = event.target.value + event.key,
            valueTreat = newValue.split('')
            
        valueTreat.forEach(el => {
            if (!el.match(/^[а-яА-Я- ]+$/)) {
                event.preventDefault()
            }
        })
    })

    $inpName.addEventListener('blur', () => {
        let validValue = $inpName.value;
        mass = [];

        validValue = validValue.split('')

        validValue.forEach(el => {
            if (!el.match(/^[а-яА-Я]+$/)) {
                el = ''
                mass.push(el)
            } else mass.push(el)
        })

        validValue = mass.toString()
        validValue = validValue.replace(/,+/g, '')
            validValue = validValue.replace(/\s+/g, ' ');
            validValue = validValue.replace(/-+/g, '-');
            
        
            if (validValue.at(0) == '-') validValue = validValue.slice(1)
            if (validValue.at(-1) == '-') validValue = validValue.slice(0, validValue.length - 1)
            validValue = validValue.trim()
            validValue = (validValue.substring(0, 1).toUpperCase() + (validValue.substring(1)).toLowerCase());
        $inpName.value = validValue
    })

    $inpSurName.addEventListener('keypress', event => {
        const newValue = event.target.value + event.key,
            valueTreat = newValue.split('')

        valueTreat.forEach(el => {
            if (!el.match(/^[а-яА-Я- ]+$/)) {
                event.preventDefault()
            }
        })
    })

    $inpSurName.addEventListener('blur', () => {
        let validValue = $inpSurName.value;
        mass = [];

        validValue = validValue.split('')

        validValue.forEach(el => {
            if (!el.match(/^[а-яА-Я]+$/)) {
                el = ''
                mass.push(el)
            } else mass.push(el)
        })

        validValue = mass.toString()
        validValue = validValue.replace(/,+/g, '')
            validValue = validValue.replace(/\s+/g, ' ');
            validValue = validValue.replace(/-+/g, '-');
            
        
            if (validValue.at(0) == '-') validValue = validValue.slice(1)
            if (validValue.at(-1) == '-') validValue = validValue.slice(0, validValue.length - 1)
            validValue = validValue.trim()
            validValue = (validValue.substring(0, 1).toUpperCase() + (validValue.substring(1)).toLowerCase());
        $inpSurName.value = validValue
    })

    $submitBtn.addEventListener('click', () => {
        const $wrapper = document.querySelector('.result-submit'),
            $FIO = document.createElement('p');

            $FIO.classList.add('result')
            $FIO.textContent = $inpLastName.value + ' ' + $inpName.value + ' ' + $inpSurName.value

            $inpLastName.value = ''
            $inpName.value = ''
            $inpSurName.value = ''

            $wrapper.append($FIO)
    })

    // Task 3

    const $scrollBtn = document.querySelector('.btn-scroll');

    window.addEventListener('scroll', () => {
        if (window.pageYOffset > 100) {
            $scrollBtn.classList.add('active')
        } else $scrollBtn.classList.remove('active')
    }, { passive: true })

    $scrollBtn.addEventListener('click', () => window.scrollTo({ top: 0, behavior: 'smooth' }))
})